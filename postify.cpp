/*This program takes each pixel and assembles a sub image to construct a image file
constructed from itself. The new image is ^4 power larger.

Postify.cpp requires grep and image magick
compile with g++ postify.cpp -o postify*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

/*imagePPM contains information about images and has methods
for loading and writing PPM with no compression. maxR maxG minB... contain the minumum
maximum and average colors for the entire photo sizeX and sizY contain the 
images size

*todo fix stratification caused by sizeX and sizeY being flipped*/
class imagePPM {
	public:
	int maxR, maxG, maxB, minR, minG, minB, sizeX, sizeY;
	int avgR=0;
	int avgG=0;
	int avgB=0;
	vector<vector<int>> red;
	vector<vector<int>> blue;
	vector<vector<int>> green;
	
	/*This function reads the file and outputs a vector storing
	Red: max min and avg Blue: max min and avg Green: max min avg*/
	void readFile(string fileName){
		cout << fileName;
		ifstream iStream(fileName);
		//Check if the file is opened
		if(!iStream.is_open()){
			cout << "Error: File Does not exist\n";
		}
		string skip;
		char s;
		int r,g,b;
		//This portion is for skipping the header of the ppm file
		iStream >> skip >> sizeX >> sizeY >> r;
		cout << skip << " "<< sizeX << " " << sizeY;
		/*This loop rights the files to the array as well as run calculations
		For the maximum minimum and average values for each color*/
		for(int i=0;i < sizeX; i++){
			red.push_back(vector<int>());
			blue.push_back(vector<int>());
			green.push_back(vector<int>());
			for(int j=0;j<sizeY and iStream >> r >> g >> b;j++){
				red.at(i).push_back(r);
				blue.at(i).push_back(g);
				green.at(i).push_back(b);
				if(r>maxR)
					maxR=r;
				if(g>maxG)
					maxG=g;
				if(b>maxB)
					maxB=b;
				if(r<minR)
					minR=r;
				if(g<minG)
					minG=g;
				if(b<minB)
					minB=b;
				avgR+=r;
				avgG+=g;
				avgB+=b;
			}
		}
		avgR/=sizeX*sizeY;
		avgG/=sizeX*sizeY;
		avgB/=sizeX*sizeY;
	}
	/*This fileWrite function writes the ppm file*/
	void fileWrite(string fileOutName){
		ofstream out;
		out.open(fileOutName);
		out << "P3" << endl << sizeX << " " << sizeY << "\n255\n"; //This line is for header data
		for(int i=0;i<sizeX;i++)
			for(int j=0;j<sizeY;j++)
				out << red.at(i).at(j) << " " << blue.at(i).at(j) << " " << green.at(i).at(j) << " ";
	}
};
/*This function takes a imagePPM object and converts it to a two tone image
The ints are for specifyign the color.*/
void imgTwoTone(imagePPM& image, int red, int blue, int green){
	//This loop is for converting the image to a black and white picture
	for(int i=0;i<image.sizeX;i++)
		for(int j=0;j<image.sizeY;j++){
			int avg=(image.red.at(i).at(j)+image.blue.at(i).at(j)+image.green.at(i).at(j))/3;
			image.red[i][j]=avg;
			image.green[i][j]=avg;
			image.blue[i][j]=avg;
	}
	//This loop changes that black and white picture into a two tone image
	for(int i=0;i<image.sizeX;i++)
		for(int j=0;j<image.sizeY;j++){
			int avg=(image.avgR+image.avgG+image.avgB)/3;
			if(image.red[i][j]<avg)
				image.red[i][j]=red/2; //This divides the input color by half
			if(image.green[i][j]<avg) //For the darker tones
				image.green[i][j]=green/2;
			if(image.blue[i][j]<avg)
				image.blue[i][j]=blue/2;
			if(image.red[i][j]>=avg)
				image.red[i][j]=red;
			if(image.green[i][j]>=avg)
				image.green[i][j]=green;
			if(image.blue[i][j]>=avg)
				image.blue[i][j]=blue;
	}
}

int main(int argc, char *argv[]){
	//Does minimal sanity check on command usage
        if (argc != 4) {
                  cerr << "Usage: posterify filename subImageX subImageY" << endl;
                  return 1;
        }

	//Converts the image to a uncompressed ppm and then removes all comments about data source
	system(("convert "+string(argv[1])+" -compress none " + "-resize " + string(argv[2]) + "x" + string(argv[3]) + " " 
		+ string(argv[1]).substr(0,string(argv[1]).find("."))+".ppm").data());
	string ppmFile=(string(argv[1]).substr(0,string(argv[1]).find("."))+".ppm");
	string(("grep -o '^[^#]*' " +  ppmFile).data());//Removes comments
	
	//Creates original image object and vector for subimages.
	imagePPM image;	
	image.readFile(ppmFile);
	vector<vector<imagePPM>> imageArray;

	//Fills out subimages
	cout << "Generating ppm images...\n";
	for(int i=0;i<image.sizeX;i++){
		imageArray.push_back(vector<imagePPM>());
		for(int j=0;j<image.sizeY;j++){
			cout << 1+i*image.sizeY+j << "/" << image.sizeX*image.sizeY  << endl; //Progress bar
			imageArray[i].push_back(image);
			imgTwoTone(imageArray.at(i).at(j),image.red[i][j],image.blue[i][j],image.green[i][j]);
			//This line is used to correct for the discrepencies in the X and Y values in the imagePPM objects
			imageArray.at(i).at(j).fileWrite("./out/"+to_string((i*image.sizeY+j)%image.sizeX)+"-"+to_string(((i*image.sizeY)+j)/image.sizeX)+".ppm");
		}
	}

	//This portion of the program stitches together the many subimages into rows and then into a single image
	cout << "Stitching them together...\n";
	for(int i=0;i<image.sizeX;i++){
		for(int j=1;j<image.sizeY;j++){
			cout << 1+i*image.sizeY+j << "/" << image.sizeX*image.sizeY  << endl; //Progress bar
			//imageArray.at(i).at(j).fileWrite("./out/"+to_string(i)+"-"+to_string(j)+".ppm");			
			//cout << image.sizeX << " " << image.sizeY << "\n";
                        system(("convert ./out/"+to_string(i)+"-0.ppm ./out/" + to_string(i)+"-"+to_string(j)+".ppm -append ./out/" +to_string(i)+"-0.ppm").data());
                        //system(("convert ./out/"+to_string(i)+"-0.ppm ./out/" + to_string(i)+"-"+to_string(j)+".ppm -append ./out/" +to_string(i)+"-0.ppm").data());
		}
	}

	//This part connects the rows
	cout << "Now for the rows...\n";
	for(int i=1;i<image.sizeX;i++){
		cout << 1+i << "/" << image.sizeX << endl;
		system(("convert ./out/0-0.ppm ./out/"+to_string(i)+"-0.ppm +append ./out/0-0.ppm").data());
	}

	//This moves the oupput file to the original directory
	system(("convert ./out/0-0.ppm " + string(argv[1]).substr(0,string(argv[1]).find("."))+"_Out.png").data());
	return 0;
}
